package com.nagarro.advjava.services;

import com.nagarro.advjava.db.DatabaseMethods;


public class LoginServiceImp implements LoginService {

	
	public boolean isUserValid(String username, String password) {
		
		boolean flag = DatabaseMethods.isUserValid(username, password);
	
		return flag;
	}	

}
