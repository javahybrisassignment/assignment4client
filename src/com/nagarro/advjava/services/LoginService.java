package com.nagarro.advjava.services;


public interface LoginService {
	
	public boolean isUserValid(String username, String password);
	
}
