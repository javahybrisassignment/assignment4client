package com.nagarro.advjava.services;

import java.util.List;

import com.nagarro.advjava.entity.Employee;

public interface EmployeeService {
	
	public List<Employee> getEmloyees();
	
	public boolean updateEmployee(Employee emp);
	
}
