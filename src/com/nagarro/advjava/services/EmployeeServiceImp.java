package com.nagarro.advjava.services;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.gson.Gson;
import com.nagarro.advjava.entity.Employee;
import com.nagarro.advjava.restconsumer.UseRestApi;

public class EmployeeServiceImp implements EmployeeService {

	public List<Employee> getEmloyees() {
		
		List<Employee> list = null;
		String response = UseRestApi.getEmployees();
		
		ObjectMapper objectMapper = new ObjectMapper();
		TypeFactory typeFactory = objectMapper.getTypeFactory();
		try {
			list = objectMapper.readValue(response, typeFactory.constructCollectionType(List.class, Employee.class));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return list;
	}

	public boolean updateEmployee(Employee emp) {
		
		boolean flag = false;
		ObjectMapper mapper = new ObjectMapper();

		//Object to JSON in String
		
		String empStr = null;
		try {
			empStr = mapper.writeValueAsString(emp);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		System.out.println("Client ----->"+empStr);
		
		String res = UseRestApi.updateEmployee(empStr);
		
		flag = res != null && res.equalsIgnoreCase("true")?true:false;
		
		return flag;
	}

}
