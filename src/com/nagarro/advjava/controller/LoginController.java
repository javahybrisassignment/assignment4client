package com.nagarro.advjava.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nagarro.advjava.services.LoginService;
import com.nagarro.advjava.services.LoginServiceImp;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/login")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	if(session.getAttribute("user") != null) {
    		response.sendRedirect("showemp");
    	}	else {
			   request.setAttribute("msg", "");
			   this.getServletConfig().getServletContext().setAttribute("username", null);
			   request.getRequestDispatcher("login.jsp").include(request, response);
		   }
    	
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");  
		  
		System.out.println( request +username + " :: " + password);
		String page = "index.jsp";
		if(username.trim().length() >= 0 && username != null &&
				password.trim().length() >= 0 && password != null) {
		  
			LoginService loginService = new LoginServiceImp();		   
			boolean flag = loginService.isUserValid(username, password);
		   
		   
		   if(flag) {
				System.out.println("Login success!!!");
				request.setAttribute("msg", "Login Success.....");
				//page = "list-employee.jsp";
				
				request.setAttribute("msg", "loggedIn Successfully");
				
				HttpSession session = request.getSession();  
		        session.setAttribute("uname",username);
		        
				response.sendRedirect("showemp");
				
		   } else {
			   request.setAttribute("msg", "Wrong Username or Password, Try again!!!");
			   this.getServletConfig().getServletContext().setAttribute("username", null);
			   request.getRequestDispatcher(page).include(request, response);
		   }
		 
		   
		}
	  
	}
	
}
