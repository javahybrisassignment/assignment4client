package com.nagarro.advjava.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nagarro.advjava.entity.Employee;
import com.nagarro.advjava.services.EmployeeService;
import com.nagarro.advjava.services.EmployeeServiceImp;

/**
 * Servlet implementation class ListEmployee
 */
@WebServlet("/showemp")
public class ListEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListEmployee() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		EmployeeService empService = new EmployeeServiceImp();
		
		List<Employee> list = empService.getEmloyees();
		
		request.setAttribute("empls", list);
		
		request.getRequestDispatcher("list-employee.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EmployeeService empService = new EmployeeServiceImp();
		
		List<Employee> list = empService.getEmloyees();
		
		request.setAttribute("empls", list);
		
		request.getRequestDispatcher("list-employee.jsp").include(request, response);
	}

}
