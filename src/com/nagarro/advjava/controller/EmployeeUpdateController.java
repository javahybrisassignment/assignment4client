package com.nagarro.advjava.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nagarro.advjava.entity.Employee;
import com.nagarro.advjava.services.EmployeeService;
import com.nagarro.advjava.services.EmployeeServiceImp;

/**
 * Servlet implementation class EmployeeUpdateController
 */
@WebServlet("/updateemp")
public class EmployeeUpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeUpdateController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.getRequestDispatcher("list-employee.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String empId = request.getParameter("empId");
		String empName = request.getParameter("empName");
		String empEmail = request.getParameter("empEmail");
		String empAddress = request.getParameter("empAddress");
		
		String dobStr = new SimpleDateFormat("yyyy-MM-dd").format(Date.valueOf(request.getParameter("empDOB")));
		
		Date empDOB = Date.valueOf(dobStr);
		
		
		System.out.println("dddd----"+empDOB);
		
		Employee emp = new Employee(empId,empName,empAddress,empEmail,empDOB);
		
		System.out.println("data dob"+emp.getEmpDOB() );
		EmployeeService empService = new EmployeeServiceImp();
		
		boolean flag = empService.updateEmployee(emp);
		
		System.out.println("Flag -- " + flag+ request.getAttribute("user"));
		 
		request.setAttribute("empId",emp.getEmpId());
		
		response.sendRedirect("login");
		//request.getRequestDispatcher("list-employee.jsp").include(request, response);
	}

}
