package com.nagarro.advjava.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nagarro.advjava.entity.Employee;

/**
 * Servlet implementation class EmployeeEditController
 */
@WebServlet("/editemp")
public class EmployeeEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeEditController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("msg", "Please enter username and password...");
		
		String empId = request.getParameter("i");
		String empName = request.getParameter("n");
		String empEmail = request.getParameter("e");
		String empAddress = request.getParameter("a");
		Date empDOB = Date.valueOf(request.getParameter("d"));
	
		Employee emp = new Employee(empId,empName,empAddress,empEmail,empDOB);
		
		request.setAttribute("emp",emp);
		
		request.setAttribute("dob",request.getParameter("d"));
		
		//response.sendRedirect(request.getContextPath() + "/"+page);
		request.getRequestDispatcher("edit-employee.jsp").include(request, response);
	}


}
