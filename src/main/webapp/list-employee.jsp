<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@page import="com.nagarro.advjava.entity.Employee"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>CRM - List Employee</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script  src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="  crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
		<link rel="stylesheet" type="text/css" href="css/login.css">
	</head>
	<body>
	<div class="wrapper">
		<!-- Header -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <a class="navbar-brand" href="#">
			    <img src="logo.png" width="90" height="30" class="d-inline-block align-top" alt="">
			    Employee CRM
 		 </a>
		
		  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
		    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		      
		      <li class="nav-item">
		        <a class="nav-link disabled" href="#"></a>
		      </li>
		    </ul>
		    <form class="form-inline my-2 my-lg-0" action="" method="post">
		      <span class="navbar-text">
			    Welcome <%=session.getAttribute("user") %>     .
			  </span>
			  <a href="index.jsp" class="btn btn-basic my-2 my-sm-0" role="button">logout</a>
		    </form>
		  </div>
		</nav>
		<!-- / Header -->
		
		<div class="container">
		<!-- / Header -->
		
			<div class="row">
			
			<div class="col-md-1"></div>
			
			<div class="col-md-10">
			
			
			
			<!-- table -->
					
					
		<div class="col-md-11 text-center">
			<h3>Employees Detail</h3>
				<div class="emp-list">
					<table class="table">
					  <thead class="thead-light">
					    <tr>
					      <th scope="col">Employee Code</th>
					      <th scope="col">Employee Name</th>
					      <th scope="col">Location</th>
					      <th scope="col">Email</th>
					      <th scope="col">Date of Birth</th>
					      <th scope="col">Action</th>
					    </tr>
					  </thead>
					  <tbody>					    
					    <% 
							List<Employee> l = (List) request.getAttribute("empls");
							int size = l.size();
							

							
						%>
						
						<% for(int i = 0; i < size; i++) {
							Employee emp = l.get(i);
							
							System.out.println("dat---: "+emp.getEmpDOB());
						%>
			            <tr>      
			                <th><%=emp.getEmpId() %></th>
			                <td><%=emp.getEmpName() %></td>
			                <td><%=emp.getEmpAddress() %></td>
			                <td><%=emp.getEmpEmail() %></td>
			                <td><%=emp.getEmpDOB() %></td>
			                <td>
			                	<a href="editemp?i=<%=emp.getEmpId() %>&n=<%=emp.getEmpName() %>&a=<%=emp.getEmpAddress() %>&e=<%=emp.getEmpEmail() %>&d=<%=emp.getEmpDOB() %>" 
			                	class="btn btn-default" role="button">Edit</a>			                
			                </td>
			            </tr>
					    <% } %>	
					    	
					  </tbody>
					</table>
				</div>
			</div>
			
			</div>
			
			<div class="col-md-1"></div>
		
			</div>
		</div>
		
		<!-- FOOTER -->
		<nav class="navbar fixed-bottom navbar-light bg-light">
		  	<span class="navbar-text">
			   Employee Management System 
			</span>
		</nav>
		<!-- /FOOTER -->
	</div>
	
</body>
</html>