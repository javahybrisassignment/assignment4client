<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@page import="com.nagarro.advjava.entity.Employee"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>CRM - Edit Employee</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script  src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="  crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
	<link rel="stylesheet" type="text/css" href="css/edit.css">
</head>
<body>
<div class="wrapper">
	<!-- Header -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <a class="navbar-brand" href="#">
			    <img src="logo.png" width="90" height="30" class="d-inline-block align-top" alt="">
			    Employee CRM
 		 </a>
		
		  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
		    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		      
		      <li class="nav-item">
		        <a class="nav-link disabled" href="#"></a>
		      </li>
		    </ul>
		    <form class="form-inline my-2 my-lg-0" action="logout" method="post">
		      <span class="navbar-text">
			    Welcome <%= session.getAttribute("user")%>    
			   </span>
		      
		      <a href="index.jsp" class="btn btn-basic my-2 my-sm-0" role="button">logout</a>
		    </form>
		  </div>
		</nav>
		<!-- / Header -->
		<div class="container">
		<!-- / Header -->
		
			<div class="row">
			
			<div class="col-md-2"></div>
			
			<div class="col-md-8">
			
				<h2> Edit Details </h2>
				<hr/>
				<div class="form-wrapper">
				<%
					Employee emp = (Employee) request.getAttribute("emp");
				%>
					<form action="updateemp" method="post">
						
						<div class="form-group">
					      <label for="">Employee Code : </label>
					      <label for=""><%=emp.getEmpId() %></label>
					    </div>
						<input type="hidden" id="custId" name="empId" value="<%=emp.getEmpId() %>">
					    <div class="form-group">
					      <label for="empName">Employee Name</label>
					      <input type="text" class="form-control" name="empName" id="empName" value="<%=emp.getEmpName() %>">
					    </div>
					    
					    <div class="form-group">
					      <label for="empLoc">Employee Address</label>
					      <textarea maxlength="500" class="form-control" name="empAddress" id="empAddress" ><%=emp.getEmpAddress() %></textarea>
					    </div>
					    
					    <div class="form-group">
					      <label for="empEmail">Employee Email ID</label>
					      <input type="email" class="form-control" name="empEmail" id="empEmail" value="<%=emp.getEmpEmail() %>">
					    </div>
					    
						  <div class="form-group">
						    <label for="empDOB">Date of Birth</label>
						    <input type="date" class="form-control" name="empDOB" id="empDOB" value="<%=request.getAttribute("dob") %>">
						  </div>
						  
						  
					  	
					  	<div class="center">
					  		<button type="submit" class="btn btn-primary">Update Details</button>
					  	</div>
					  					  		
					  
					</form>
				</div>			
			
			</div>
			
			<div class="col-md-2"></div>
		
			</div>
		</div>
		
		<!-- FOOTER -->
		<nav class="navbar fixed-bottom navbar-light bg-light">
		  	<span class="navbar-text">
			   Employee Management System 
			</span>
		</nav>
		<!-- /FOOTER -->
	</div>
</body>
</html>