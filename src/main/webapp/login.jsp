    
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>CRM - Login</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script  src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="  crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
		<link rel="stylesheet" type="text/css" href="css/login.css">
	</head>
	<body>
		<div class="wrapper">
		<!--  Header -->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <a class="navbar-brand" href="#">
			    <img src="logo.png" width="110" height="30" class="d-inline-block align-top" alt="">
			    Employee CRM
 		 </a>
		</nav>	
		<div class="container">
		<!-- / Header -->
		
			<div class="row">
			
			<div class="col-md-1"></div>
			
			<div class="col-md-10">
			
			<!-- Login Form -->		
		
				<div class="login-form">
				    <form action="login" method="post">
				        <h2 class="text-center">Log in</h2>       
				        <div class="form-group">
				            <input type="text" class="form-control" placeholder="Username" name="username" required="required">
				        </div>
				        <div class="form-group">
				            <input type="password" class="form-control" placeholder="Password" name="password" required="required">
				        </div>
				        <div class="form-group">
				            <button type="submit" class="btn btn-primary btn-block">Log in</button>
				        </div>
				        <div class="clearfix">
				        <% String msg = "";
				        if (request.getAttribute("msg")  != null) 
				        	msg = (String)request.getAttribute("msg");
				        %>
				            <h6><%= msg %></h6>
				        </div>        
				    </form>
				</div>
			
			<!-- /Login Form -->
			
			</div>
			
			<div class="col-md-1"></div>
		
			</div>
		</div>
		<!-- FOOTER -->
		<nav class="navbar fixed-bottom navbar-light bg-light">
		  	<span class="navbar-text">
			   Employee Management System 
			</span>
		</nav>
		<!-- /FOOTER -->
	</div>		
	</body>
</html>